FROM mcr.microsoft.com/dotnet/core/aspnet:3.1.0-alpine3.10

WORKDIR /usr/src/scblifenetcoreapp

COPY ./bin/Release/netcoreapp3.1/publish ./

EXPOSE 80

ENTRYPOINT [ "dotnet", "scblife-netcore.dll"]