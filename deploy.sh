sshpass  -p $1 ssh -o StrictHostKeyChecking=no root@157.245.206.146 << ENDSSH
   docker login -u $2 -p $3 $4
   docker stop $6
   docker rm $6
   docker pull $5
   docker run --restart=always -d --name $6 -p 9727:80 $5
ENDSSH